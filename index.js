require('dotenv').config()
const express = require('express')
const cors = require('cors')
const path = require('path')
const fs = require('fs')
const axios = require('axios')
const compression = require('compression')
const tar = require('tar-stream')
const zlib = require('zlib')

const app = express()

app.use(compression())
app.use(cors())

app.get('/:pkg/:version', async (req, res) => {
  const pkg = req.params.pkg
  const version = req.params.version
  const mode = (req.query && req.query.mode) || 'js'
  const scope = req.query && req.query.scope ? `@${req.query.scope}/` : ''
  const url = `${process.env.REGISTRY_URL}/${scope}${pkg}/${version}`
  let pkgUrl
  let pkgVersion

  const extract = tar.extract()
  let chunks = []

  extract.on('entry', function (header, stream, next) {
    if (header.name.includes(mode == 'js' ? pkg : 'style')) {
      stream.on('data', function (chunk) {
        chunks.push(chunk)
      })
    }

    stream.on('end', function () {
      next()
    })

    stream.resume()
  })

  extract.on('finish', function () {
    if (chunks.length) {
      const data = Buffer.concat(chunks)

      fs.writeFileSync(file, data)
      res.sendFile(path.join(__dirname, file))
    }
  })

  try {
    const response = await axios.get(url)
    pkgUrl = response.data.dist.tarball
    pkgVersion = response.data.version
  } catch (e) {
    return res
      .status(404)
      .json({ package: pkg, version, error: 'Package not found' })
  }

  const folder = 'tmp'
  const file =
    mode == 'js'
      ? `${folder}/${pkg}-${pkgVersion}.js`
      : `${folder}/${pkg}-${pkgVersion}.css`

  if (!fs.existsSync(folder)) {
    fs.mkdirSync(folder)
  }

  if (fs.existsSync(file)) {
    res.sendFile(path.join(__dirname, file))
  }

  try {
    const response = await axios.get(pkgUrl, { responseType: 'stream' })
    response.data.pipe(zlib.createGunzip()).pipe(extract)
  } catch (e) {
    return res.status(500).json({
      package: pkg,
      version: pkgVersion,
      error: `Something went wrong when downloading tgz file`,
    })
  }
})

app.listen(process.env.PORT, () =>
  console.log(`listening on ${process.env.PORT}`)
)
