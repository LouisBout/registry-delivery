# REGISTRY DELIVERY

A tool that communicates with a private npm registry (Verdaccio) and returns js or css file

## Why

I needed to make libraries (webcomponents) accessible to several apps and avoid at the same time updating them on each project.
I also needed to have the choice to change version whenever i wanted.

## Installation

```shell
yarn
```

## Prepare environments

Before starting the server, you need to create a .env file.

### Available variables
- **REGISTRY_URL** ==> private registry url (example: http://localhost:4873)
- **PORT** ==> port that will be used by the server

## Start the server

```shell
yarn serve
```

## Prerequisites for libraries

As this project is intended to be used with webcomponents libraries, you have to do the following steps on your libs projects

- Make sure to run build command
- The style file must be style.css and your lib js filename has to be equal to the lib name (package.json)

You can add the following code to your package.json to specify the files you want to publish to your npm registry

```json
"files": ["dist/*.js", "dist/*.css"]
```

Now you can publish your webcomponents lib to the registry.

```shell
npm publish
```


## How does it work ?

Once the server started, you will be able to use the following api endpoint

### /:component/:version

- You can pass the query param `mode` to the url to specify if you want either a js file or css file
- You can also pass the query param `scope` to the url to specify the scope where the package is referenced => if the package is @capsulecorp/my-super-lib, you'll have to use this parameter otherwise it is not necessary.

#### Available options for mode param
- js
- css

**Full example**: http://localhost:3000/my-super-lib/latest?scope=capsulecorp&mode=css


the server will extract library tgz file from registry and store it in project under the folder /tmp only if the specified version is not found otherwise the file is directly picked from tmp folder. Finally the file is sent through the api endpoint.
